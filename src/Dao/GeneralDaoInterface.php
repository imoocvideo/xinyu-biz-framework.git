<?php

namespace Codeages\Biz\Framework\Dao;

interface GeneralDaoInterface extends DaoInterface
{
    public function create($fields);

    public function update($id, array $fields);

    public function delete($id);

    /**
     * 根据id获取一条记录
     * @param $id
     * @param bool $lock true 表示 Select for update
     * @return mixed
     */
    public function get($id, $lock = false);

    public function search($conditions, $orderbys, $start, $limit);

    public function count($conditions);

    public function wave(array $ids, array $diffs);
}
