<?php

namespace Codeages\Biz\Framework\Service;

use Codeages\Biz\Framework\Context\Biz;
use Xiaofubao\Context\XiaofubaoKernel;

abstract class BaseService
{
    /**
     * @var XiaofubaoKernel
     */
    protected $kernel;

    public function __construct(Biz $biz)
    {
        $this->kernel = $biz;
    }
}
